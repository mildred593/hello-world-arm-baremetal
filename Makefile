all: test.bin

run:
	qemu-system-arm -M versatilepb -m 128M -nographic -kernel test.bin

debug:
	qemu-system-arm -M versatilepb -m 128M -nographic -s -S -kernel test.bin

.PHONY: all run debug

%.o: %.s
	arm-none-eabi-as -mcpu=arm926ej-s -g $< -o $@

%.o: %.c
	arm-none-eabi-gcc -c -mcpu=arm926ej-s -g $< -o $@

test.elf: test.o startup.o test.ld
	arm-none-eabi-ld -T test.ld test.o startup.o -o test.elf

test.bin: test.elf
	arm-none-eabi-objcopy -O binary test.elf test.bin
